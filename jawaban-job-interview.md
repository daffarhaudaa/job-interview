# 1. Monitoring resource dari komputer

**RAM**
>  Untuk memonitor RAM kita bisa menggunakan command `free`kemudian tekan enter, kemudian akan muncul tampilan sebagai berikut :

![dokumentasi](dokumentasi/ram.jpg)

Pada bagian ini terlihat dengan jelas ukuran memori laptop kita. berapa ukuranya akan terlihat dengan jelas. Memori yang saya gunakan sebanyak  1,9Gi alias 2Gb.

Untuk melihat tampilan RAM lebih rinci bisa menggunakan command `free --mega` atau `free --giga`, dapat dilihat pada tampilan berikut :

![dokumentasi](dokumentasi/ram.jpg)

**CPU**
> Untuk memonitor penggunaan CPU, bisa menggunakan command `top` atau `ps -aux` lalu tekan enter, kemudian akan muncul tampilan sebagai berikut :

“top” adalah aplikasi di Linux yang dijalankan melalui Terminal di Linux. Aplikasi ini digunakan untuk memanajemen proses Linux tanpa menggunakan GUI. Saat dieksekusi, aplikasi ini akan menampilkan daftar semua proses yang sedang berjalan dan setiap detik akan diperbaharui. Proses yang ditampilkan pada perintah “top” adalah yang paling besar menggunakan sumber daya.

![dokumentasi](dokumentasi/top.gif)

“ps” adalah aplikasi di Linux yang digunakan untuk menampilkan aktif proses yang berjalan pada sistem. Aplikasi ini dapat Anda gunakan untuk melakukan manajemen proses Linux. Sedangkan jika ingin melihat informasi proses secara keseluruhan biasanya menggunakan perintah “ps -aux”.

![dokumentasi](dokumentasi/ps_-aux.gif)

**STORAGE**
> Untuk memonitor penggunaan Storage, bisa menggunakan command `df` atau `lsblk` lalu tekan enter. df (Disk Free)
digunakan untuk melihat report sisa ruang disk space pada linux sistem ,untuk manual penggunaan nya silakan gunakan perintah command df sedangkan lsblk (List Block Devices) digunakan untuk menampilkan block device yang tersedia,namun tidak menampilkan RAM , block device yang dimaksud disini seperti flashdisk,mmc,HDD,SSD,DCROM dll.untuk manual penggunaan nya silakan gunakan perintah command lsblk. Maka akan muncul tampilan sebagai berikut :

![dokumentasi](dokumentasi/storage.gif)

# 2. Manajemen Program

**MEMONITOR PROGRAM YANG BERJALAN**
> Salah satu perintah yang paling banyak digunakan untuk monitoring performance Linux adalah command Top. Command ini berfungsi untuk menampilkan kinerja sistem secara real time seperti CPU, Memory, Serta proses yang sedang berjalan. Untuk memonitor program yang berjalan, bisa menggunakan command yang sama seperti sebelumnya yaitu menggunakan `top, htop`, atau `btop`. Berikut tampilannya :

![dokumentasi](dokumentasi/top.gif)

**MENGHENTIKAN PROGRAM YANG BERJALAN**
> Kita biasa menghentikan program atau aplikasi yang berajalan dengan mengklik tombol X (close) atau Alt+F4. Nah di Linux kita bisa menghentikan program dengan perintah kill di terminal. Untuk menggunakan perinta kill ini kita harus mengetahui PID dari program yang sedang berjalan. Setiap program yang berjalan pasti memiliki PID, PID ini bersifat uniq artinya setiap program berbeda. Untuk menghentikan program yang berjalan bisa dengan cara menggunakan command `top` terlebih dahulu kemudian masukan `keyword k`, lalu masukan nomer `PID` nya dan yang terakhir masukkan sinyalnya, sinyal yang umum digunakan diantaranya : 

- 1 (-HUP): untuk restart suatu proses.
- 9 (-KILL): untuk mematikan suatu proses.
- 15 (-TERM): untuk menghentikan proses dengan cara berbeda.

![dokumentasi](dokumentasi/kill.gif)

**OTOMASI PERINTAH DENGAN SHELL SCRIPT**
> Shell Script merupakan file yang berisikan serangkaian perintah untuk dieksekusi oleh program berbasis command line. Langkah pertama kita buat sebuah file scriptnya dengan menggunakan command `touch`, lalu setelah berhasil dibuat gunakan command `vim`. Selanjutnya tinggal memasukkan perintah-perintah bash yang akan diotomasi. Untuk mengeksekusi program tersebut bisa menggunakan `./ namafile.sh <option>`. Tampilannya dapat dilihat sebagai berikut : 

![dokumentasi](dokumentasi/shellscript.gif)

**PENJADWALAN EKSEKUSI PROGRAM MENGGUNAKAN CRON**
> Cron adalah tool yang memungkinkan user menginput command (perintah untuk menjadwalkan tugas berulang pada waktu tertentu. Cron job adalah tugas yang dijadwalkan di cron. User bisa menentukan tugas yang mereka inginkan untuk dijalankan secara otomatis beserta waktu eksekusinya. Untuk membuat atau mengedit file crontab, kita bisa menggunakan command `crontab -e`. Berikut contohnya :

![dokumentasi](dokumentasi/crontab.gif)

Maka program otomatisasi tersebut akan berjalan sesuai dengan waktu yang telah ditentukan, dan dapat dilihat seperti gambar dibawah ini 

![dokumentasi](dokumentasi/crontabhasil.jpg)

# 3. Manajemen Network

**MENGAKSES SISTEM OPERASI PADA JARINGAN MENGGUNAKAN SSH**
> SSH, atau Secure Shell, adalah suatu protokol yang digunakan untuk masuk secara aman ke sistem jauh. Inilah cara paling umum untuk mengakses server jauh Linux. Cara mengakses sistem operasi pada jaringan yaitu bisa dengan cara ketik `ssh`, lalu `remote_username@remote_host` atau `IP address` kemudian bisa dengan menggunakan port (Port ini berfungsi mengirimkan data melalui jaringan dalam bentuk terenkripsi) atau tidak. Berikut contohnya :

![dokumentasi](dokumentasi/ssh.gif)

![dokumentasi](dokumentasi/port.gif)

**MEMONITOR PROGRAM MENGGUNAKAN NETWORK**
> Beberapa program menggunakan port sebagai jalur komunikasinya dengan eksternal. Netstat adalah program command line untuk cek aktivitas network. Untuk memonitor program menggunakan network bisa dengan cara command `netstat -tulpn` Dapat dilihat sebagai berikut :

![dokumentasi](dokumentasi/netstat.gif)

**MENGOPERASIKAN HTTP CLIENT**
> HTTP Client berfungsi untuk memanggil sebuah data dari jarak jauh baik itu outputnya berupa image ataupun text dengan command `wget` atau `curl -o`. Berikut contohnya :

![dokumentasi](dokumentasi/curl.gif)

![dokumentasi](dokumentasi/wget.gif)

# 4. Manajemen File dan Folder

**NAVIGASI**
>  Untuk navigasi bisa menggunakan command berikut: 

- cd, untuk mengganti direktori 
- ls, menampilkan semua list file dan folder yang ada di direktori yang sedang ditempati 
- pwd, menampilkan direktori yang sedang ditempati

![dokumentasi](dokumentasi/navigasi.gif)

**CRUD FILE DAN FOLDER**
> Untuk CRUD bisa menggunakan command berikut: 

- mkdir, untuk membuat direktori 
- touch, untuk membuat file
- rm, menghapus file atau direktori 
- mv, cut/move file atau direktori 
- cp, copy file atau direktori 

![dokumentasi](dokumentasi/crud.gif)

**MANAJEMEN OWNERSHIP**
> Ada dua command dasar dalam manajemen ownership yaitu chmod dan chown. 
Berikut ini adalah contoh penggunaannya: 

chmod [option] 
- chmod, digunakan untuk memodifikasi permission suatu file.
- [option] berisi perintah untuk mengubah permission 
- berisi nama file yang akan diubah permissionnya.

![dokumentasi](dokumentasi/chmod.gif)

chown [user:group]
- chown, digunakan untuk memodifikasi owner dari suatu file. 
- [user:group], berisi nama user/group mana yang akan menjadi ownernya. 
- berisi nama file yang akan diubah ownernya. 

![dokumentasi](dokumentasi/chwon.gif)

Gambar diatas terjadi permission not permited dikarenakan owner tersebut tidak mengizinkan untuk mengubah hak akses ownernya.

**PENCARIAN FILE DAN FOLDER**
> Untuk mencari file dan folder bisa menggunakan find . Contoh penggunaan find:
-  diisi dengan memasukkan direktori yang akan dicari file dan foldernya. 
-  diisi dengan kondisi yang akan dipilih. 
-  diisi dengan actions yang akan dilakukan. 

![dokumentasi](dokumentasi/find.gif)

**KOMPRESI DATA**
> Tar merupakan singkatan dari Tape archive dan digunakan untuk mengompres serangkaian file dan folder.
Umumnya, file yang telah di-compress dengan menggunakan command `tar` akan tersimpan dalam bentuk `file .tar.` Kompresi selanjutnya yang menggunakan `gzip` akan menghasilkan file dalam format `.tar.gz.`. Berikut contohn penggunaanya :

![dokumentasi](dokumentasi/tar-cf.gif)

![dokumentasi](dokumentasi/tar-xf.gif)

Jika Anda menginginkan proses kompresi yang lebih baik, gunakan `.tar.gz`. Penambahan opsi `z` merepresentasikan kompresi gzip. Berikut contohnya :

![dokumentasi](dokumentasi/gzip.gif)

# 5. Shell Script

![dokumentasi](dokumentasi/shellscript.gif)

# 6. Link YouTube
> https://youtu.be/7GaWITqUywo

# 7. Maze Game
> Untuk Maze Game bisa dilihat sebagai berikut :

![dokumentasi](dokumentasi/maze.gif)

atau bisa juga di demokan pada link dibawah

https://maze.informatika.digital/maze/arara/
